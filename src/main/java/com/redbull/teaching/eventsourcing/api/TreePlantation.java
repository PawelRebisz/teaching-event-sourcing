package com.redbull.teaching.eventsourcing.api;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

public interface TreePlantation {
    @RequestMapping(value = "/actions/plant", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    ResponseEntity<TreePlantationResponseDto> plantATree(@RequestBody PlantATreeDto dto);

    @RequestMapping(value = "/actions/cutdown", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    ResponseEntity<TreePlantationResponseDto> cutDownATree(@RequestBody CutDownATreeDto dto);

    @RequestMapping(value = "/actions/mark", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    ResponseEntity<TreePlantationResponseDto> markATree(@RequestBody MarkATreeDto dto);
}
