package com.redbull.teaching.eventsourcing.domain;

import com.redbull.teaching.eventsourcing.api.CutDownATreeDto;
import lombok.Getter;

@Getter
class CutDownATreeEvent implements Event {

    private final String treeName;
    private final String reason;

    private CutDownATreeEvent(String treeName, String reason) {
        this.treeName = treeName;
        this.reason = reason;
    }

    static Event from(CutDownATreeDto dto) {
        return new CutDownATreeEvent(
            dto.getTreeName(),
            dto.getReason()
        );
    }
}
