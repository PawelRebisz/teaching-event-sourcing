package com.redbull.teaching.eventsourcing.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.redbull.teaching.eventsourcing.domain.journaling.Snapshotable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class PlantationRepository implements Snapshotable, Printable {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlantationRepository.class);
    private final Map<String, Tree> treesInPlantation = new HashMap<>();

    void plantATree(PlantATreeEvent event) {
        String treeName = event.getTreeName();
        Tree seekedTree = new Tree(treeName);
        if (treesInPlantation.containsKey(treeName)) {
            throw new TreeAlreadyPlantedException(seekedTree);
        }
        treesInPlantation.put(treeName, seekedTree);
    }

    void cutDownATree(CutDownATreeEvent event) {
        String treeName = event.getTreeName();
        Tree seekedTree = new Tree(event.getTreeName());
        if (!treesInPlantation.containsKey(treeName)) {
            throw new TreeIsMissingException(seekedTree);
        }
        Tree foundTree = treesInPlantation.get(treeName);
        if (!foundTree.isMarkedToCutDown()) {
            throw new TreeNotMarkedToCutDownException(seekedTree);
        }
        treesInPlantation.remove(treeName);
    }


    void markATree(MarkATreeEvent event) {
        String treeName = event.getTreeName();
        Tree seekedTree = new Tree(event.getTreeName());
        if (!treesInPlantation.containsKey(treeName)) {
            throw new TreeIsMissingException(seekedTree);
        }
        Tree foundTree = treesInPlantation.get(treeName);
        foundTree.markToBeCutDown();
    }

    @Override
    public void snapshot() {
        try {
            FileWriter fileWriter = new FileWriter(Paths.get("snapshot-" + System.currentTimeMillis() + ".snapshot").toFile());
            String snapshot = treesInPlantation
                    .values()
                    .stream()
                    .map(Tree::asJsonString)
                    .collect(Collectors.toList()).toString();
            fileWriter.append(snapshot);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @Override
    public void loadFromSnapshot(File snapshot) {
        LOGGER.info("Loading plantation from snapshot {}.", snapshot);
        ObjectMapper mapper = new ObjectMapper();
        try {
            List<Map<String, Object>> snapshotedTrees = mapper.readValue(snapshot, List.class);
            for (Map<String, Object> snapshotedTree : snapshotedTrees) {
                Tree tree = new Tree((String) snapshotedTree.get("name"));
                if((boolean) snapshotedTree.get("marked")) {
                    tree.markToBeCutDown();
                }
                treesInPlantation.put(tree.getName(), tree);
            }
        } catch (IOException exception) {
            throw new IllegalStateException(exception);
        }
    }

    @Override
    public void print() {
        LOGGER.info("PlantationRepository: {}",
            treesInPlantation
                .values()
                .stream()
                .map(Tree::asJsonString)
                .collect(Collectors.toList()));
    }
}
