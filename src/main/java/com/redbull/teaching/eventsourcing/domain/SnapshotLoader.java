package com.redbull.teaching.eventsourcing.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.springframework.util.StringUtils.isEmpty;

public class SnapshotLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(SnapshotLoader.class);

    public SnapshotLoader(String snapshotPath, PlantationRepository plantationRepository) {
        if (isEmpty(snapshotPath)) {
            LOGGER.info("No snapshot was provided, hence no loading is performed.");
            return;
        }

        Path path = Paths.get(snapshotPath);
        plantationRepository.loadFromSnapshot(path.toFile());
    }
}
