package com.redbull.teaching.eventsourcing.domain.journaling;

import java.io.File;

public interface Snapshotable {
    void snapshot();

    void loadFromSnapshot(File snapshot);
}
