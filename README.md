# Event sourcing

Supporting example for presentation: https://docs.google.com/presentation/d/1c_UErXq6ueS7rSdmr5S6lvpBCpfJD3WebEQ91v1ouLA

### Small disclaimer
Since this project is for showing the concept, not so many tests can be found ;) but as with all spiky projects I think it is excused.

Also for the sake of the example this code is totally not thread safe, but in the end it was not the intention there.