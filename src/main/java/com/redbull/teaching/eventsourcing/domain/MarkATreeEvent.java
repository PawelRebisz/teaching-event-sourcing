package com.redbull.teaching.eventsourcing.domain;

import com.redbull.teaching.eventsourcing.api.MarkATreeDto;
import lombok.Getter;

@Getter
class MarkATreeEvent implements Event {

    private final String treeName;

    private MarkATreeEvent(String treeName) {
        this.treeName = treeName;
    }

    static Event from(MarkATreeDto dto) {
        return new MarkATreeEvent(dto.getTreeName());
    }
}
