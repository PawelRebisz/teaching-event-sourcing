package com.redbull.teaching.eventsourcing.domain;

import com.redbull.teaching.eventsourcing.api.TreePlantation;
import com.redbull.teaching.eventsourcing.domain.journaling.JournalingService;
import com.redbull.teaching.eventsourcing.domain.journaling.PoorMansFileBasedJournalingService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class ApplicationConfiguration {

    @Value("${snapshot:#{null}}")
    private String snapshot;

    @Value("${journal:#{null}}")
    private String journal;

    @Bean
    JournalingService journalingService() throws IOException {
        return new PoorMansFileBasedJournalingService();
    }

    @Bean
    PlantationRepository plantationRepository() {
        return new PlantationRepository();
    }

    @Bean
    PlantationService plantationService(PlantationRepository plantationRepository) {
        return new PlantationService(plantationRepository);
    }

    @Bean
    RequestDtoTransformer requestDtoTransformer() {
        return new RequestDtoTransformer();
    }

    @Bean
    SnapshotLoader repositoryLoader(PlantationRepository plantationRepository) {
        return new SnapshotLoader(snapshot, plantationRepository);
    }

    @Bean
    JournalLoader journalLoader(TreePlantation treePlantation) throws IOException {
        return new JournalLoader(journal, treePlantation);
    }
}
