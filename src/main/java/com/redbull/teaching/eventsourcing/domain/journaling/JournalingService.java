package com.redbull.teaching.eventsourcing.domain.journaling;

public interface JournalingService {
    boolean journal(Journalable dto);
}
