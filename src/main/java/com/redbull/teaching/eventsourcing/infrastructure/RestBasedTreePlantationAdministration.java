package com.redbull.teaching.eventsourcing.infrastructure;

import com.redbull.teaching.eventsourcing.api.TreePlantationResponseDto;
import com.redbull.teaching.eventsourcing.domain.AdministrationService;
import com.redbull.teaching.eventsourcing.domain.PlantationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/system")
class RestBasedTreePlantationAdministration {

    private final AdministrationService administrationService;

    @Autowired
    public RestBasedTreePlantationAdministration(PlantationService administrationService) {
        this.administrationService = administrationService;
    }

    @RequestMapping(value = "/actions/snapshot", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<TreePlantationResponseDto> takeSystemSnapshot() {
        administrationService.takeSystemSnapshot();
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(TreePlantationResponseDto.success());
    }

    @RequestMapping(value = "/actions/print", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<TreePlantationResponseDto> printSystemStatus() {
        administrationService.printSystemStatus();
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(TreePlantationResponseDto.success());
    }
}
