package com.redbull.teaching.eventsourcing.domain;

public interface Printable {
    void print();
}
