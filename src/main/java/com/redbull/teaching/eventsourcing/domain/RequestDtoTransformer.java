package com.redbull.teaching.eventsourcing.domain;

import com.redbull.teaching.eventsourcing.api.CutDownATreeDto;
import com.redbull.teaching.eventsourcing.api.MarkATreeDto;
import com.redbull.teaching.eventsourcing.api.PlantATreeDto;

public class RequestDtoTransformer {

    public Event intoEvent(Object dto) {
        switch(dto.getClass().getSimpleName()) {
            case "PlantATreeDto":
                return PlantATreeEvent.from((PlantATreeDto) dto);
            case "MarkATreeDto":
                return MarkATreeEvent.from((MarkATreeDto) dto);
            case "CutDownATreeDto":
                return CutDownATreeEvent.from((CutDownATreeDto) dto);
            default:
                throw new IllegalArgumentException("Received dto with unrecognized type.");
        }
    }
}
