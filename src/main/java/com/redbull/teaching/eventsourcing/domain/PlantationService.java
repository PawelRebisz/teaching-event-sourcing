package com.redbull.teaching.eventsourcing.domain;

public class PlantationService implements AdministrationService {

    private final PlantationRepository plantationRepository;

    public PlantationService(PlantationRepository plantationRepository) {
        this.plantationRepository = plantationRepository;
    }

    public void plantATree(Event event) {
        plantationRepository.plantATree(
            (PlantATreeEvent) event
        );
    }

    public void cutDownATree(Event event) {
        plantationRepository.cutDownATree(
            (CutDownATreeEvent) event
        );
    }

    public void markATree(Event event) {
        plantationRepository.markATree(
            (MarkATreeEvent) event
        );
    }

    @Override
    public void takeSystemSnapshot() {
        plantationRepository.snapshot();
    }

    @Override
    public void printSystemStatus() {
        plantationRepository.print();
    }
}
