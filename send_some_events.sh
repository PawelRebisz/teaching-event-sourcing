#!/bin/bash

curl -H "Content-Type: application/json" -X POST --data '{ "name": "Oak tree" }' http://localhost:8080/trees/actions/plant
curl -H "Content-Type: application/json" -X POST --data '{ "name": "Pine tree" }' http://localhost:8080/trees/actions/plant
curl -H "Content-Type: application/json" -X POST --data '{ "name": "Oak 2 tree" }' http://localhost:8080/trees/actions/plant
curl -H "Content-Type: application/json" -X POST --data '{ "name": "Oak tree" }' http://localhost:8080/trees/actions/mark
curl -H "Content-Type: application/json" -X POST --data '{ "name": "Oak 2 tree", "reason": "Very old tree :("}' http://localhost:8080/trees/actions/cutdown
curl -H "Content-Type: application/json" -X POST --data '{ "name": "Oak 3 tree" }' http://localhost:8080/trees/actions/plant
curl -H "Content-Type: application/json" -X POST --data '{ "name": "Pine 2 tree" }' http://localhost:8080/trees/actions/plant
curl -H "Content-Type: application/json" -X POST --data '{ "name": "Oak tree" }' http://localhost:8080/trees/actions/plant