package com.redbull.teaching.eventsourcing.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import org.json.JSONObject;

@EqualsAndHashCode
class Tree {

    private final String name;
    private boolean markedToCutDown;

    Tree(String name) {
        this.name = name;
        this.markedToCutDown = false;
    }

    @JsonProperty("name")
    String getName() {
        return name;
    }

    @JsonProperty("markedToCutDown")
    boolean isMarkedToCutDown() {
        return markedToCutDown;
    }

    void markToBeCutDown() {
        markedToCutDown = true;
    }

    String asJsonString() {
        return new JSONObject()
            .put("name", name)
            .put("marked", markedToCutDown)
            .toString();
    }
}
