package com.redbull.teaching.eventsourcing.domain;

class TreeIsMissingException extends RuntimeException {
    private final Tree tree;

    TreeIsMissingException(Tree tree) {
        super("Tree " + tree.getName() + " is not present in the plantation.");
        this.tree = tree;
    }
}
