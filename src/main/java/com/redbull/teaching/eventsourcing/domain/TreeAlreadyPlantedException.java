package com.redbull.teaching.eventsourcing.domain;

class TreeAlreadyPlantedException extends RuntimeException {
    private final Tree tree;

    TreeAlreadyPlantedException(Tree tree) {
        super("Tree " + tree.getName() + " is already planted on the plantation.");
        this.tree = tree;
    }
}
