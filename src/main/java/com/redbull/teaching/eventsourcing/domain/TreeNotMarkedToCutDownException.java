package com.redbull.teaching.eventsourcing.domain;

class TreeNotMarkedToCutDownException extends RuntimeException {
    private final Tree tree;

    TreeNotMarkedToCutDownException(Tree tree) {
        super("Tree " + tree.getName() + " is not marked to be cut down in the plantation.");
        this.tree = tree;
    }
}
