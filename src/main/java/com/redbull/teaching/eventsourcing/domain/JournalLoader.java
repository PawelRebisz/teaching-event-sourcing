package com.redbull.teaching.eventsourcing.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.redbull.teaching.eventsourcing.api.CutDownATreeDto;
import com.redbull.teaching.eventsourcing.api.MarkATreeDto;
import com.redbull.teaching.eventsourcing.api.PlantATreeDto;
import com.redbull.teaching.eventsourcing.api.TreePlantation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import static org.springframework.util.StringUtils.isEmpty;

class JournalLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(JournalLoader.class);
    private final ObjectMapper mapper = new ObjectMapper();

    JournalLoader(String journalPath, final TreePlantation treePlantation) {
        if (isEmpty(journalPath)) {
            LOGGER.info("No journal was provided, hence no loading is performed.");
            return;
        }

        LOGGER.info("Loading plantation from journal {}.", journalPath);
        Path path = Paths.get(journalPath);
        try {
            BufferedReader reader = Files.newBufferedReader(path);
            String line;
            while ((line = reader.readLine()) != null) {
                String eventAsJsonString = line.substring(20);
                process(treePlantation, eventAsJsonString);
            }
        } catch (IOException exception) {
            throw new IllegalStateException(exception);
        }
    }

    private void process(TreePlantation treePlantation, String eventAsJsonString) throws IOException {
        Map<String, Object> json = mapper.readValue(eventAsJsonString, Map.class);
        switch ((String) json.get("type")) {
            case "PLANT_A_TREE":
                treePlantation.plantATree(new PlantATreeDto(
                        (String) json.get("name")
                ));
                break;
            case "MARK_A_TREE":
                treePlantation.markATree(new MarkATreeDto(
                        (String) json.get("name")
                ));
                break;
            case "CUT_DOWN_A_TREE":
                treePlantation.cutDownATree(new CutDownATreeDto(
                        (String) json.get("name"),
                        (String) json.get("reason")
                ));
                break;
            default:
                throw new IllegalArgumentException("Event read from journal with unrecognized type.");
        }
    }
}
