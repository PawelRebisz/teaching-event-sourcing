package com.redbull.teaching.eventsourcing.domain;

public interface AdministrationService {
    void takeSystemSnapshot();

    void printSystemStatus();
}
