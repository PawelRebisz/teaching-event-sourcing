package com.redbull.teaching.eventsourcing.domain.journaling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.format.DateTimeFormatter;

import static java.time.LocalDateTime.now;

public class PoorMansFileBasedJournalingService implements JournalingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PoorMansFileBasedJournalingService.class);
    private static final char END_OF_LINE = '\n';

    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
    private final FileWriter fileWriter;

    public PoorMansFileBasedJournalingService() throws IOException {
        fileWriter = new FileWriter(Paths.get("journal-" + System.currentTimeMillis() + ".journal").toFile());
    }

    @Override
    public boolean journal(Journalable dto) {
        LOGGER.info("Journaling dto {}.", dto.journal());
        try {
            fileWriter
                .append(now().format(dateTimeFormatter))
                .append(" ")
                .append(dto.journal())
                .append(END_OF_LINE);
            fileWriter.flush();
            return true;
        } catch (Exception exception) {
            LOGGER.info("Failed to journal dto {} due to {}.", dto, exception.getMessage(), exception);
            return false;
        }
    }
}
