package com.redbull.teaching.eventsourcing.domain.journaling;

public interface Journalable {

    String journal();
}
