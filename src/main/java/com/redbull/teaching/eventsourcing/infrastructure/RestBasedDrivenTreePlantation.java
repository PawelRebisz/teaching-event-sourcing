package com.redbull.teaching.eventsourcing.infrastructure;

import com.redbull.teaching.eventsourcing.api.*;
import com.redbull.teaching.eventsourcing.domain.Event;
import com.redbull.teaching.eventsourcing.domain.PlantationService;
import com.redbull.teaching.eventsourcing.domain.RequestDtoTransformer;
import com.redbull.teaching.eventsourcing.domain.journaling.Journalable;
import com.redbull.teaching.eventsourcing.domain.journaling.JournalingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.function.Consumer;

import static com.redbull.teaching.eventsourcing.api.TreePlantationResponseDto.error;

@RestController
@RequestMapping("/trees")
class RestBasedDrivenTreePlantation implements TreePlantation {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestBasedDrivenTreePlantation.class);

    private final RequestDtoTransformer requestDtoTransformer;
    private final JournalingService journalingService;
    private final PlantationService plantationService;

    @Autowired
    public RestBasedDrivenTreePlantation(RequestDtoTransformer requestDtoTransformer,
                                         JournalingService journalingService,
                                         PlantationService plantationService) {
        this.requestDtoTransformer = requestDtoTransformer;
        this.journalingService = journalingService;
        this.plantationService = plantationService;
    }

    @Override
    @RequestMapping(value = "/actions/plant", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<TreePlantationResponseDto> plantATree(@RequestBody PlantATreeDto dto) {
        return journalEventAndProcess(
            dto,
            plantationService::plantATree
        );
    }

    @Override
    @RequestMapping(value = "/actions/cutdown", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<TreePlantationResponseDto> cutDownATree(@RequestBody CutDownATreeDto dto) {
        return journalEventAndProcess(
            dto,
            plantationService::cutDownATree
        );
    }

    @Override
    @RequestMapping(value = "/actions/mark", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<TreePlantationResponseDto> markATree(@RequestBody MarkATreeDto dto) {
        return journalEventAndProcess(
            dto,
            plantationService::markATree
        );
    }

    private ResponseEntity<TreePlantationResponseDto> journalEventAndProcess(Journalable dto, Consumer<Event> consumer) {
        boolean success = journalingService.journal(dto);
        if (success) {
            Event event = requestDtoTransformer.intoEvent(dto);
            try {
                consumer.accept(event);
            } catch (Exception exception) {
                LOGGER.error("Failed to process the request due to: {}", exception.getMessage(), exception);
                return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body(error());
            }
            return ResponseEntity.ok(TreePlantationResponseDto.success());
        } else {
            LOGGER.error("Failed to journal received request {}.", dto);
            return ResponseEntity
                    .status(HttpStatus.CONFLICT)
                    .body(error());
        }
    }
}
