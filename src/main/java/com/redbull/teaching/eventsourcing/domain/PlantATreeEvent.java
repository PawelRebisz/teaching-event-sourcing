package com.redbull.teaching.eventsourcing.domain;

import com.redbull.teaching.eventsourcing.api.PlantATreeDto;
import lombok.Getter;

@Getter
class PlantATreeEvent implements Event {

    private final String treeName;

    private PlantATreeEvent(String treeName) {
        this.treeName = treeName;
    }

    static Event from(PlantATreeDto dto) {
        return new PlantATreeEvent(dto.getTreeName());
    }
}
