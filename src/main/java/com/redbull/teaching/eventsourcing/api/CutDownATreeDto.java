package com.redbull.teaching.eventsourcing.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.redbull.teaching.eventsourcing.domain.journaling.Journalable;
import lombok.Getter;
import org.json.JSONObject;

@Getter
public class CutDownATreeDto implements Journalable {

    private final String treeName;
    private final String reason;

    @JsonCreator
    public CutDownATreeDto(@JsonProperty("name") String treeName, @JsonProperty("reason") String reason) {
        this.treeName = treeName;
        this.reason = reason;
    }

    @Override
    public String journal() {
        return new JSONObject()
                .put("type", "CUT_DOWN_A_TREE")
                .put("name", treeName)
                .put("reason", reason)
                .toString();
    }
}
