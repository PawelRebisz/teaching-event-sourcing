package com.redbull.teaching.eventsourcing.api;

import lombok.Getter;

@Getter
public class TreePlantationResponseDto {
    private final String status;

    private TreePlantationResponseDto(String status) {
        this.status = status;
    }

    public static TreePlantationResponseDto success() {
        return new TreePlantationResponseDto("success");
    }

    public static TreePlantationResponseDto error() {
        return new TreePlantationResponseDto("error");
    }
}
