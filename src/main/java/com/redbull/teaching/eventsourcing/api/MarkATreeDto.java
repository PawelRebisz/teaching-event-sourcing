package com.redbull.teaching.eventsourcing.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.redbull.teaching.eventsourcing.domain.journaling.Journalable;
import lombok.Getter;
import org.json.JSONObject;

@Getter
public class MarkATreeDto implements Journalable {

    private final String treeName;

    @JsonCreator
    public MarkATreeDto(@JsonProperty("name") String treeName) {
        this.treeName = treeName;
    }

    @Override
    public String journal() {
        return new JSONObject()
            .put("type", "MARK_A_TREE")
            .put("name", treeName)
            .toString();
    }
}
